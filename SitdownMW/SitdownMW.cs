﻿using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;

namespace SitdownMW
{
    public partial class SitdownMW : ServiceBase
    {
        private static System.Timers.Timer checkMW;
        public SitdownMW()
        {
            InitializeComponent();
            eventLogHandler = new System.Diagnostics.EventLog();
            //System.Diagnostics.EventLog.DeleteEventSource("SitdownMW");
            if (!System.Diagnostics.EventLog.SourceExists("SitdownMW"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "SitdownMW", "Sit down Modern Warfare");
            }
            eventLogHandler.Source = "SitdownMW";
            eventLogHandler.Log = "Sit down Modern Warfare";
        }

        protected override void OnStart(string[] args)
        {
            checkMW = new System.Timers.Timer(10000); //Once every 10 seconds.
            checkMW.Elapsed += Trigger;
            checkMW.AutoReset = true;
            checkMW.Enabled = true;
            eventLogHandler.WriteEntry("Now monitoring for ModernWarfare.");
        }

        private void Trigger(object sender, ElapsedEventArgs e)
        {
            //Locate Modern Warfare executables.
            Process[] pname = Process.GetProcessesByName("ModernWarfare");
            if (pname.Length != 0)
            {
                foreach (var process in Process.GetProcessesByName("ModernWarfare"))
                {
                    //Is it already set to normal?
                    if (process.PriorityClass != ProcessPriorityClass.Normal)
                    {
                        eventLogHandler.WriteEntry("Telling Modern Warfare to sit down!");
                        process.PriorityClass = ProcessPriorityClass.Normal; //Sit!
                    }
                }
            }
        }
        protected override void OnStop()
        {
            checkMW.Stop();
            eventLogHandler.WriteEntry("No longer monitoring Modern Warfare.");
        }
    }
}
