﻿using System;
using System.IO;
using System.Security.Cryptography;
using YamlDotNet.RepresentationModel;

namespace Package_Manifest
{
    class Program
    {
        private static string GetChecksum(string file)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] checksum = sha.ComputeHash(stream);
                return BitConverter.ToString(checksum).Replace("-", String.Empty);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Generating manifest.");
            var stream = new YamlStream(
                new YamlDocument(
                    new YamlMappingNode(
                        new YamlScalarNode("Id"), new YamlScalarNode("AshleyStone.SitdownMW"),
                        new YamlScalarNode("Publisher"), new YamlScalarNode("Ashley Stone"),
                        new YamlScalarNode("Version"), new YamlScalarNode("1.0.0.0"),
                        new YamlScalarNode("AppMoniker"), new YamlScalarNode("SitdownMW"),
                        new YamlScalarNode("Name"), new YamlScalarNode("Sit down Modern Warfare"),
                        new YamlScalarNode("Description"), new YamlScalarNode("Automatically lowers Modern Warfare priority."),
                        new YamlScalarNode("Homepage"), new YamlScalarNode("https://bitbucket.org/AshleyStone/sitdownmw/"),
                        new YamlScalarNode("License"), new YamlScalarNode("Copyright Ashley Stone. All rights reserved."),
                        new YamlScalarNode("InstallerType"), new YamlScalarNode("msi"),
                        new YamlScalarNode("Installers"), new YamlSequenceNode(
                            new YamlMappingNode(
                                new YamlScalarNode("Arch"), new YamlScalarNode("neutral"),
                                new YamlScalarNode("Scope"), new YamlScalarNode("machine"),
                                new YamlScalarNode("Url"), new YamlScalarNode("https://bitbucket.org/AshleyStone/sitdownmw/downloads/SitdownMW-1.0.0.0.msi"),
                                new YamlScalarNode("Sha256"), new YamlScalarNode(GetChecksum(@"..\..\..\..\Installer\bin\Release\SitdownMW.msi"))
                            )
                        )
                    )
                )
            );
            stream.Save(Console.Out, assignAnchors: false);
            Console.WriteLine("Saving winget-Package_Manifest.yaml.");
            using (TextWriter writer = File.CreateText(@".\winget-Package_Manifest.yaml"))
            {
                stream.Save(writer, assignAnchors: false);
            }
            Console.WriteLine("Done.");
        }
    }
}
